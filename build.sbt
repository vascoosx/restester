name := "restester"

version := "1.0"

scalaVersion in ThisBuild  := "2.11.8"

lazy val root = project in file(".") aggregate(resgetter, resprober)

lazy val resgetter = project in file("resgetter")
lazy val resprober = project in file("reprober") dependsOn(resgetter)

