package what.ever

import com.typesafe.config.ConfigFactory
import net.ceedubs.ficus.Ficus._
import net.ceedubs.ficus.readers.ArbitraryTypeReader._

object ApplicationSetting {

  val env = sys.env.getOrElse("DEV_ENV", "default")
  val config = {
    ConfigFactory.defaultOverrides
      .withFallback(ConfigFactory.load(env))
      .withFallback(ConfigFactory.load)
  }

  case class AWS(accessKey: String,
                 secretKey: String)

  val aws = config.as[AWS]("aws")

}
