name := "resgetter"

scalaVersion  := "2.11.8"

organization := "what.ever"

version := "1.0"

libraryDependencies ++= Seq(
  "net.ceedubs" %% "ficus" % "1.1.2",
  "com.typesafe" % "config" % "1.2.1"
)
